package neogoji

import (
	"fmt"
	"log"

	"github.com/neo4j/neo4j-go-driver/neo4j"
)

func useConsoleLogger(level neo4j.LogLevel) func(config *neo4j.Config) {
	return func(config *neo4j.Config) {
		config.Log = neo4j.ConsoleLogger(level)
	}
}

type Neogoji interface {
	Close()
	Push(interface{}, map[string]func(interface{}) (interface{}, error)) error
}

type neogoji struct {
	conf   Config
	driver neo4j.Driver
}

func (n *neogoji) Close() {
	n.Close()
}

func New(conf ...Config) (n Neogoji, err error) {
	var (
		d neo4j.Driver
		c Config
	)

	if len(conf) != 0 {
		c = conf[0]
	}
	err = c.Fill()
	if err != nil {
		return
	}
	d, err = neo4j.NewDriver(
		fmt.Sprintf("bolt://%s:%s", c.Host, c.Port),
		neo4j.BasicAuth(c.Name, c.Pass, ""),
		useConsoleLogger(neo4j.ERROR),
	)
	if err != nil {
		log.Println("Error creating neogoji")
		return
	}
	n = &neogoji{driver: d}
	return
}
