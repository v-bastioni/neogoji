package main

import (
	"time"

	"gitlab.com/v-bastioni/neogoji"
	"gitlab.com/v-bastioni/neogoji/models/relation"
	"log"
)

type Accord struct {
	ToSkip     int
	Labels     []string  `neo4j:"labels"`
	DateDiff   time.Time `neo4j:"acc_date_diff"`
	Title      string    `neo4j:"acc_titre"`
	DateEffet  time.Time `neo4j:"acc_date_effet"`
	DateMaj    time.Time `neo4j:"acc_date_maj"`
	DateSign   time.Time `neo4j:"acc_date_sign"`
	DateFin    time.Time `neo4j:"acc_date_fin"`
	ID         string    `neo4j:"acc_num"`
	Nature     string    `neo4j:"acc_nature"`
	IsIntegral bool      `neo4j:"acc_is_integral"`
	Path       string    `neo4j:"acc_path"`
}

type Puce struct {
	Beau     bool                `neo4j:"beau"`
	Age      int                 `neo4j:"age"`
	Labels   []string            `neo4j:"labels"`
	Cherie   string              `neo4j:"cherie"`
	Relation []relation.Relation `neo4j:"relation"`
}

type Cherie struct {
	Name     string              `neo4j:"name"`
	Age      int                 `neo4j:"age"`
	Labels   []string            `neo4j:"labels"`
	Belle    bool                `neo4j:"belle"`
	Aimante  bool                `neo4j:"aimante"`
	Relation []relation.Relation `neo4j:"relation"`
	NewOne   string              `neo4j:"test,true,test_func"`
}

func main() {
	d, err := neogoji.New()
	if err != nil {
		log.Fatal(err)
	}
	puce := Puce{
		Labels: []string{"PUCEPUCE", "PERSON", "PUCE_TULLE"},
		Beau:   true,
		Age:    29,
		Cherie: "Clemence",
	}
	cherie := Cherie{
		Labels:  []string{"CHERIE"},
		Name:    "Clemence",
		Age:     26,
		Belle:   true,
		Aimante: true,
		NewOne:  "ok boy",
	}

	//? NB: could be done programmatically via Reflection
	rel := relation.New("LOVES", &puce, &cherie)
	puce.Relation = []relation.Relation{rel}
	cherie.Relation = []relation.Relation{rel}
	err = d.Push(&puce, map[string]func(interface{}) (interface{}, error){
		"test_func": func(interface{}) (interface{}, error) {
			return "this is the return of the test func", nil
		},
	})
	if err != nil {
		log.Fatal(err)
	}
}
