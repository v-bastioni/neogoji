package neogoji

import (
	"errors"
	"os"
)

var (
	ErrMissingHost = errors.New("missing neo4j host")
	ErrMissingPort = errors.New("missing neo4j port")
	ErrMissingName = errors.New("missing neo4j name")
	ErrMissingPass = errors.New("missing neo4j pass")
)

type Config struct {
	Host, Port, Name, Pass string
}

func (c *Config) Fill() error {
	var ok bool
	for _, k := range []struct {
		ptr      *string
		key, def string
		err      error
	}{
		{ptr: &c.Host, key: "NEO4J_HOST", err: ErrMissingHost, def: "localhost"},
		{ptr: &c.Port, key: "NEO4J_PORT", err: ErrMissingPort, def: "7687"},
		{ptr: &c.Name, key: "NEO4J_NAME", err: ErrMissingName, def: "neo4j"},
		{ptr: &c.Pass, key: "NEO4J_PASS", err: ErrMissingPass},
	} {
		if *k.ptr == "" {
			*k.ptr, ok = os.LookupEnv(k.key)
			if !ok {
				if k.def == "" {
					return k.err
				}
				*k.ptr = k.def
			}
		}
	}
	return nil
}
