package neogoji

import (
	"fmt"
	"gitlab.com/v-bastioni/neogoji/models/gatherer"
	"reflect"
	"regexp"
	"strconv"
	"strings"
)

var tagRegexp = regexp.MustCompile(`(?m)neo4j:"([A-Za-z0-9_,]+)"`)

type ErrMissingConverter struct {
	Tag string
}

func (e *ErrMissingConverter) Error() string {
	return fmt.Sprintf("missing converter for '%s'", e.Tag)
}

type ErrInvalidTagFormat struct {
	Faulty, Details string
}

func (e *ErrInvalidTagFormat) Error() string {
	s := fmt.Sprintf("invalid tag format '%s'", e.Faulty)
	if e.Details != "" {
		s += " " + e.Details
	}
	return s
}

type Tag struct {
	Value     string
	Set       bool
	Converter string
}

func splitTag(s string, g gatherer.Gatherer) (t Tag, err error) {
	splitted := strings.Split(s, ",")
	l := len(splitted)
	switch l {
	case 3:
		t.Converter = splitted[2]
		fallthrough
	case 2:
		t.Set, err = strconv.ParseBool(splitted[1])
		if err != nil {
			err = &ErrInvalidTagFormat{s, "invalid second argument"}
			return
		}
		fallthrough
	case 1:
		t.Value = splitted[0]
	default:
		err = &ErrInvalidTagFormat{s, "invalid count"}
		return
	}
	return
}

func extractTag(tag reflect.StructTag, g gatherer.Gatherer) (t Tag, err error) {
	s := string(tag)
	if !strings.HasPrefix(s, "neo4j") {
		t = Tag{Value: s}
		return
	}
	res := tagRegexp.FindStringSubmatch(s)
	if len(res) != 2 {
		err = &ErrInvalidTagFormat{Faulty: s}
		return
	}
	t, err = splitTag(res[1], g)
	return
}
