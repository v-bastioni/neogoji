# start from golang image based on alpine-3.8
FROM golang:1.10-alpine3.8 AS dev-build

# add our cgo dependencies
RUN apk add --update --no-cache ca-certificates cmake make g++ openssl-dev git curl pkgconfig
# clone seabolt-1.7.0 source code
RUN git clone -b 1.7 https://github.com/neo4j-drivers/seabolt.git /seabolt
# invoke cmake build and install artifacts - default location is /usr/local
WORKDIR /seabolt/build
# CMAKE_INSTALL_LIBDIR=lib is a hack where we override default lib64 to lib to workaround a defect
# in our generated pkg-config file 
RUN cmake -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_LIBDIR=lib .. && cmake --build . --target install

# install dep
RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

WORKDIR /go/src/app
ADD . /go/src/app

# install dependencies
RUN dep init

CMD ["go", "run", "."]

# Install Dependencies
RUN go get github.com/ampersandlabs-io/jwt-auth-filter
RUN go get github.com/bradfitz/gomemcache/memcache
RUN go get github.com/gorilla/mux
RUN go get github.com/paulmach/go.geojson
RUN go get gopkg.in/mgo.v2/bson
RUN go get github.com/joho/godotenv
RUN go get github.com/lib/pq
RUN go get github.com/rs/cors
RUN go get github.com/neo4j/neo4j-go-driver/neo4j

CMD ["go", "run", "main.go"]
