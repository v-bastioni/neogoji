package main

import (
	"reflect"
	"regexp"
	"time"

	"github.com/neo4j/neo4j-go-driver/neo4j"
)

var (
	tagRegexp = regexp.MustCompile(`(?m)neo4j:"([A-Za-z0-9_]+)"`)
)

func extractTag(tag reflect.StructTag) string {
	res := tagRegexp.FindStringSubmatch(string(tag))
	if len(res) != 2 {
		return string(tag)
	}
	return res[1]
}

func Parse(n neo4j.Node, host interface{}, str_to_time func(string) (time.Time, error)) error {
	vof := reflect.ValueOf(host).Elem()
	tof := reflect.TypeOf(host).Elem()
	for i := 0; i < vof.NumField(); i++ {
		vfield := vof.Field(i)
		tfield := tof.Field(i)
		tt := extractTag(tfield.Tag)
		switch tt {
		case "": // pretty much nothing to do here except skip it
		case "labels":
			labels := n.Labels()
			l := len(labels)
			slice := reflect.MakeSlice(reflect.TypeOf([]string{}), l, l)
			for i := 0; i < l; i++ {
				slice.Index(i).SetString(labels[i])
			}
			vfield.Set(slice)
		default:
			fields := n.Props()
			switch vfield.Interface().(type) {
			case string:
				switch v := fields[tt].(type) {
				case string:
					vfield.SetString(v)
				}
			case int:
				switch v := fields[tt].(type) {
				case int:
					vfield.SetInt(int64(v))
				}
			case time.Time:
				switch v := fields[tt].(type) {
				case string:
					tm, err := str_to_time(v)
					if err != nil {
						return err
					}
					vfield.Set(reflect.ValueOf(tm))
				case time.Time:
					vfield.Set(reflect.ValueOf(v))
				}
			case bool:
				switch v := fields[tt].(type) {
				case bool:
					vfield.SetBool(v)
				}
			}
		}
	}
	return nil
}
