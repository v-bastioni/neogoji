package main

// // GetEnvOrElse tries to get a key value pair from the Env global var,
// // if it fails, returns the given `def` (default) value given
// func GetEnvOrElse(key, def string) string {
// 	if v, ok := os.LookupEnv(key); ok {
// 		return v
// 	}
// 	return def
// }
