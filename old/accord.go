package main

import "time"

type Accord struct {
	ToSkip     int
	Labels     []string  `neo4j:"labels"`
	DateDiff   time.Time `neo4j:"acc_date_diff"`
	Title      string    `neo4j:"acc_titre"`
	DateEffet  time.Time `neo4j:"acc_date_effet"`
	DateMaj    time.Time `neo4j:"acc_date_maj"`
	DateSign   time.Time `neo4j:"acc_date_sign"`
	DateFin    time.Time `neo4j:"acc_date_fin"`
	ID         string    `neo4j:"acc_num"`
	Nature     string    `neo4j:"acc_nature"`
	IsIntegral bool      `neo4j:"acc_is_integral"`
	Path       string    `neo4j:"acc_path"`
}
