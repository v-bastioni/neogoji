package main

import (
	"fmt"
	"log"
	"os"
	"reflect"
	"regexp"
	"time"

	"github.com/neo4j/neo4j-go-driver/neo4j"
)

var (
	tagRegexp = regexp.MustCompile(`(?m)neo4j:"([A-Za-z0-9_]+)"`)
)

func extractTag(tag reflect.StructTag) string {
	res := tagRegexp.FindStringSubmatch(string(tag))
	if len(res) != 2 {
		return string(tag)
	}
	return res[1]
}

type Accord struct {
	ToSkip     int
	Labels     []string  `neo4j:"labels"`
	DateDiff   time.Time `neo4j:"acc_date_diff"`
	Title      string    `neo4j:"acc_titre"`
	DateEffet  time.Time `neo4j:"acc_date_effet"`
	DateMaj    time.Time `neo4j:"acc_date_maj"`
	DateSign   time.Time `neo4j:"acc_date_sign"`
	DateFin    time.Time `neo4j:"acc_date_fin"`
	ID         string    `neo4j:"acc_num"`
	Nature     string    `neo4j:"acc_nature"`
	IsIntegral bool      `neo4j:"acc_is_integral"`
	Path       string    `neo4j:"acc_path"`
}

// GetEnvOrElse tries to get a key value pair from the Env global var,
// if it fails, returns the given `def` (default) value given
func GetEnvOrElse(key, def string) string {
	if v, ok := os.LookupEnv(key); ok {
		return v
	}
	return def
}

var (
	neo4jHost = GetEnvOrElse("NEO4J_HOST", "localhost")
	neo4jPort = GetEnvOrElse("NEO4J_PORT", "7687")
	neo4jName = GetEnvOrElse("NEO4J_NAME", "neo4j")
	neo4jPass = GetEnvOrElse("NEO4J_PASS", "neo4j")
)

type Etab struct {
	CreationData time.Time `neo4j:"etab_date_crea"`
	IsSiege      bool      `neo4j:"etab_is_siege"`
	Code         string    `neo4j:"etab_code"`
	Denomination string    `neo4j:"etab_denomination"`
	Siret        string    `neo4j:"siret"`
}

func str_to_time(s string) (time.Time, error) {
	return time.Parse("2006-01-02", s)
}

func test_etab(session neo4j.Session) error {
	const req = "MATCH (e:Etablissement)-[:SIGNED]-(a:Accord) RETURN e, a LIMIT 10"

	var (
		result neo4j.Result
		err    error
	)

	result, err = session.Run(req, map[string]interface{}{})
	if err != nil {
		return err
	}

	for result.Next() {
		if rec, ok := result.Record().Get("e"); ok {
			if node, ok := rec.(neo4j.Node); ok {
				fmt.Printf("%v\n", node)
				etab := Etab{}
				Parse(node, &etab, str_to_time)
				fmt.Printf("%v\n", etab)
			}
		}
		if rec, ok := result.Record().Get("a"); ok {
			if node, ok := rec.(neo4j.Node); ok {
				accord := Accord{}
				Parse(node, &accord, str_to_time)
				fmt.Printf("%v\n", accord)
			}
		}
	}

	return nil
}

func test_accord(session neo4j.Session) error {
	fmt.Printf("testing accords\n")
	const req = "MATCH (a:Accord) RETURN {acc_date_diff: a.acc_date_diff} AS a LIMIT 1"
	var (
		result neo4j.Result
		err    error
	)

	result, err = session.Run(req, map[string]interface{}{})
	// result, err = session.Run("MATCH (a:Accord { acc_num: $acc_num }) RETURN a", map[string]interface{}{
	// 	"acc_num": "T59L19003458",
	// })
	if err != nil {
		return err // handle error
	}

	for result.Next() {
		fmt.Println("HERE")
		if rec, ok := result.Record().Get("a"); ok {
			fmt.Println("HERE 2")
			fmt.Printf("rec: %v (type: %T)\n", rec, rec)
			if node, ok := rec.(neo4j.Node); ok {
				fmt.Println("HERE 3")
				accord := Accord{}
				Parse(node, &accord, str_to_time)
				fmt.Printf("%v\n", accord)
			} else {
				fmt.Println("NOT OK")
			}
		}
	}
	if err = result.Err(); err != nil {
		return err // handle error
	}
	return nil
}

func run() error {
	var (
		driver  neo4j.Driver
		session neo4j.Session
		err     error
	)

	if driver, err = neo4j.NewDriver(
		fmt.Sprintf("bolt://%s:%s", neo4jHost, neo4jPort),
		neo4j.BasicAuth(neo4jName, neo4jPass, ""),
	); err != nil {
		return err // handle error
	}
	// handle driver lifetime based on your application lifetime requirements
	// driver's lifetime is usually bound by the application lifetime, which usually implies one driver instance per application
	defer driver.Close()

	if session, err = driver.Session(neo4j.AccessModeWrite); err != nil {
		return err
	}
	defer session.Close()

	// if err = test_accord(session); err != nil {
	// 	return err
	// }
	if err = test_etab(session); err != nil {
		return err
	}
	return nil
}

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}
