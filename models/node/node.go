package node

import (
	"fmt"
	"strings"

	"gitlab.com/v-bastioni/neogoji/models/relation"
)

type Attribute struct {
	Value interface{}
	Set   bool
}

type InternalNode interface {
	ID() int
	UUID() uintptr

	SetID(id int)

	Label() []string
	Attributes() map[string]Attribute
	Relations() []relation.Relation

	AddLabel(...string)
	AddAttribute(string, Attribute)
	AddRelation(...relation.Relation)

	Merge() string
	Create() string
}

type internalNode struct {
	id         int
	uuid       uintptr
	labels     []string
	attributes map[string]Attribute
	relations  []relation.Relation
}

func New(uuid uintptr) InternalNode {
	return &internalNode{
		uuid:       uuid,
		labels:     []string{},
		attributes: map[string]Attribute{},
	}
}

func (in *internalNode) ID() int                          { return in.id }
func (in *internalNode) UUID() uintptr                    { return in.uuid }
func (in *internalNode) Label() []string                  { return in.labels }
func (in *internalNode) Attributes() map[string]Attribute { return in.attributes }
func (in *internalNode) Relations() []relation.Relation   { return in.relations }

func (in *internalNode) SetID(id int) { in.id = id }

func (in *internalNode) AddLabel(labels ...string) {
	in.labels = append(in.labels, labels...)
}

func (in *internalNode) AddAttribute(key string, value Attribute) {
	in.attributes[key] = value
}

func (in *internalNode) AddRelation(relations ...relation.Relation) {
	in.relations = append(in.relations, relations...)
}

func (in *internalNode) Create() string {
	return in.createReq("CREATE")
}

func (in *internalNode) Merge() string {
	return in.createReq("MERGE")
}

func mapKeys(id int, m map[string]interface{}) []string {
	strs := []string{}
	for k, _ := range m {
		strs = append(strs, fmt.Sprintf("%s: $n%03d_%s", k, id, k))
	}
	return strs
}

func splitKeys(attributes map[string]Attribute) (attrs, sets map[string]interface{}) {
	attrs = map[string]interface{}{}
	sets = map[string]interface{}{}
	for k, v := range attributes {
		if v.Set {
			sets[k] = v.Value
		} else {
			attrs[k] = v.Value
		}
	}
	return
}

func (in *internalNode) createReq(kw string) string {
	labels := ""
	for _, l := range in.labels {
		labels += ":" + l
	}
	attrs, sets := splitKeys(in.attributes)
	attributes := strings.Join(mapKeys(in.id, attrs), ", ")
	identifier := fmt.Sprintf("n%03d", in.id)
	s := fmt.Sprintf("%s (%s%s { %s })", kw, identifier, labels, attributes)
	if len(sets) > 0 {
		s += "\n"
	}
	for k, _ := range sets {
		s += fmt.Sprintf("SET %[1]s.%[2]s = $%[1]s_%[2]s\n", identifier, k)
	}
	return s
}
