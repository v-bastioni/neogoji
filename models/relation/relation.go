package relation

type Relation interface {
	Label() string
	From() interface{}
	To() interface{}
}

type relation struct {
	label    string
	from, to interface{}
}

func (l *relation) From() interface{} { return l.from }
func (l *relation) To() interface{}   { return l.to }
func (l *relation) Label() string     { return l.label }

func New(label string, from, to interface{}) Relation {
	return &relation{label, from, to}
}
