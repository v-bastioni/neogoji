package gatherer

import (
	"fmt"
	"reflect"

	"gitlab.com/v-bastioni/neogoji/models/node"
	"gitlab.com/v-bastioni/neogoji/models/relation"
)

type Gatherer interface {
	PushElement(node.InternalNode) int
	PushRelation(relation.Relation) bool

	Merge() (string, map[string]interface{})
	Create() (string, map[string]interface{})

	GetConverter(tag string) func(interface{}) (interface{}, error)
}

type gatherer struct {
	counter    int
	mapped     map[uintptr]node.InternalNode
	relations  map[uintptr]relation.Relation
	converters map[string]func(interface{}) (interface{}, error)
}

func (g *gatherer) PushElement(n node.InternalNode) (num int) {
	id := n.UUID()
	_, ok := g.mapped[id]
	if !ok {
		g.mapped[id] = n
		n.SetID(g.counter)
		g.counter++
	}
	return n.ID()
}

func (g *gatherer) PushRelation(r relation.Relation) bool {
	addr := reflect.ValueOf(r).Pointer()
	_, ok := g.relations[addr]
	if !ok {
		g.relations[addr] = r
	}
	return !ok
}

func (g *gatherer) GetConverter(tag string) func(interface{}) (interface{}, error) {
	c, ok := g.converters[tag]
	if !ok {
		return nil
	}
	return c
}

func New(converters map[string]func(interface{}) (interface{}, error)) Gatherer {
	if converters == nil {
		converters = map[string]func(interface{}) (interface{}, error){}
	}
	return &gatherer{
		counter:    0,
		mapped:     map[uintptr]node.InternalNode{},
		relations:  map[uintptr]relation.Relation{},
		converters: converters,
	}
}

func (g *gatherer) getReq(c string, f func(n node.InternalNode) func() string) (req string, params map[string]interface{}) {
	params = map[string]interface{}{}
	req = ""
	for _, e := range g.mapped {
		req += f(e)()
		req += "\n"
		for k, v := range e.Attributes() {
			newK := fmt.Sprintf("n%03d_%s", e.ID(), k)
			params[newK] = v.Value
		}
	}
	i := 0
	for _, r := range g.relations {
		f := r.From()
		t := r.To()
		faddr := reflect.ValueOf(f).Pointer()
		taddr := reflect.ValueOf(t).Pointer()
		fnode := g.mapped[faddr]
		tnode := g.mapped[taddr]
		req += fmt.Sprintf(
			"\n%s (n%03d)-[r%03d:%s]->(n%03d)\n",
			c,
			fnode.ID(),
			i, r.Label(),
			tnode.ID(),
		)
		i++
	}
	return
}

func (g *gatherer) Merge() (req string, params map[string]interface{}) {
	return g.getReq("MERGE", func(n node.InternalNode) func() string { return n.Merge })
}

func (g *gatherer) Create() (req string, params map[string]interface{}) {
	return g.getReq("CREATE", func(n node.InternalNode) func() string { return n.Create })
}
