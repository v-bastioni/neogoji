package neogoji

import (
	"errors"
	"reflect"
	"time"

	"github.com/neo4j/neo4j-go-driver/neo4j"
	"gitlab.com/v-bastioni/neogoji/models/gatherer"
	"gitlab.com/v-bastioni/neogoji/models/node"
	"gitlab.com/v-bastioni/neogoji/models/relation"
)

var (
	ErrInvalidLabel    = errors.New("invalid label")
	ErrInvalidRelation = errors.New("invalid relation")
)

func strToTime(s string) (time.Time, error) {
	return time.Parse("2006-01-02", s)
}

func timeToStr(t time.Time) string {
	return t.Format("2006-01-02")
}

func getRelation(g gatherer.Gatherer, in node.InternalNode, id uintptr, vf reflect.Value, _ Tag) (err error) {
	links, ok := vf.Interface().([]relation.Relation)
	if !ok {
		err = ErrInvalidRelation
		return
	}

	in.AddRelation(links...)
	for _, rel := range links {
		l := rel.From()
		laddr := reflect.ValueOf(l).Pointer()
		r := rel.To()
		raddr := reflect.ValueOf(r).Pointer()
		if g.PushRelation(rel) {
			if laddr != id {
				if err = ExtractNode(l, g); err != nil {
					return
				}
			}
			if raddr != id {
				if err = ExtractNode(r, g); err != nil {
					return
				}
			}
		}
	}
	return
}

func getDefault(g gatherer.Gatherer, in node.InternalNode, _ uintptr, vf reflect.Value, tag Tag) (err error) {
	v := vf.Interface()
	if tag.Converter != "" {
		f := g.GetConverter(tag.Converter)
		if f == nil {
			err = &ErrMissingConverter{Tag: tag.Value}
			return
		}
		v, err = f(v)
		if err != nil {
			return
		}
	}
	in.AddAttribute(tag.Value, node.Attribute{Value: v, Set: tag.Set})
	return
}
func getLabels(_ gatherer.Gatherer, in node.InternalNode, _ uintptr, vf reflect.Value, _ Tag) (err error) {
	labels, ok := vf.Interface().([]string)
	if !ok {
		err = ErrInvalidLabel
		return
	}
	in.AddLabel(labels...)
	return
}

func ExtractNode(n interface{}, g gatherer.Gatherer) (err error) {
	vof := reflect.ValueOf(n).Elem()
	tof := reflect.TypeOf(n).Elem()
	ln := vof.NumField()
	id := reflect.ValueOf(n).Pointer()
	in := node.New(id)

	for i := 0; i < ln; i++ {
		var (
			tf  = tof.Field(i)
			vf  = vof.Field(i)
			tag Tag
		)
		tag, err = extractTag(tf.Tag, g)
		if err != nil {
			return
		}
		switch tag.Value {
		case "":
		case "labels":
			if err = getLabels(g, in, id, vf, tag); err != nil {
				return
			}
		case "relation":
			if err = getRelation(g, in, id, vf, tag); err != nil {
				return
			}
		default:
			if err = getDefault(g, in, id, vf, tag); err != nil {
				return
			}
		}
	}
	g.PushElement(in)
	return
}

func (n *neogoji) Push(e interface{}, conversions map[string]func(interface{}) (interface{}, error)) (err error) {
	var (
		sess neo4j.Session
		r    neo4j.Result
		g    = gatherer.New(conversions)
	)

	err = ExtractNode(e, g)
	if err != nil {
		return err
	}
	req, params := g.Merge()
	sess, err = n.driver.Session(neo4j.AccessModeWrite)
	if err != nil {
		return
	}
	defer sess.Close()
	r, err = sess.Run(req, params)
	if err != nil {
		return
	}
	_, err = r.Summary()
	if err != nil {
		return
	}
	return nil
}
